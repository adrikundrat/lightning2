import csv
import pandas as pd
import numpy as np
import math 
import os
import tempfile
import cv2
import json
import zipfile,fnmatch
from PIL import Image, ImageDraw
from os import path

#### ak822dj
########################
#### I know to read and parse zooniverse json annotations into readable table
#### Also used segmentation to get result if lightning is over/under 2kHz
########################

data=pd.read_csv("/Users/adrian/OneDrive - Technicka univerzita v Kosiciach/skola3BP/BP/vystupy_anot/jjmarkers-classifications2.csv")
data=data.drop(data.index[0:50])
root="/Users/adrian/OneDrive - Technicka univerzita v Kosiciach/skola3BP/BP/clean_annotations/"

with open('/Users/adrian/OneDrive - Technicka univerzita v Kosiciach/skola3BP/BP/vystupy_anot/tabulka9.csv', mode='w') as csv_file:
    fieldnames = ['event','signal', 'date', 'second', 'milisecond', 'tweek signs', 'f_signs [kHz]', 'f_min<2kHz']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()

    a=0
    eventnr=0
    while (a<=len(data)-1):
        filename=json.loads(data.iloc[a,12])[str(data.iloc[a,13])]["Filename"]
        filename1="1"+filename

        if path.exists(root+filename1):
            value=json.loads(data.iloc[a, 11])[0]["value"]
            if (json.loads(data.iloc[a, 10])["subject_dimensions"][0]):
                widthms=(json.loads(data.iloc[a, 10])["subject_dimensions"][0]["naturalWidth"])/1000
            eventnr+=1
            date=filename[4:19]
            sec=filename[filename.find("s")+1:filename.find(".png")]
            sig=0
            for k in value:
                if len(k["details"])>0:
                    twsigns=k["details"][0]["value"]
                else: twsigns=None
                im = Image.open(os.path.join(root, filename1))
                im = im.crop((k["x"], 0, k["x"]+k["width"], 162)).save(os.path.join(root, "1.png"))
                image=cv2.imread(os.path.join(root, "1.png"))

                r = image.copy()
                r[:, :, 0] = 0
                r[:, :, 1] = 0
                b=r[:, :, 2]

                b=np.delete(b,len(b)-1,0)
                b=np.delete(b,len(b)-1,0)

                transposeB=np.transpose(b)
                middle=0
                indexOfMid=0
                currentIndex=1
                for i in transposeB:
                    if sum(i) > middle:
                        middle=sum(i)
                        indexOfMid=currentIndex
                    print(sum(i))
                    currentIndex+=1

                print(indexOfMid+k["x"])
                milsec=round((indexOfMid+k["x"])/widthms)

                j=0
                celyobr=0
                while(j<160):
                    celyobr+=sum(b[j])
                    j+=1

                j=0
                konec=0
                while(j<20):
                    konec+=sum(b[139+j])
                    j+=1

                if (float((konec*100)/celyobr))>9.5:
                    vysledok=1
                else: vysledok=0
                
                sig+=1
                writer.writerow({'event': eventnr,'signal':sig, 'date': date, 'second': sec, 'milisecond': milsec, 'tweek signs': twsigns, 
                    'f_signs [kHz]': None, 'f_min<2kHz': vysledok})
                print(filename1+" done")
        a+=1



#284553872,not-logged-in-16fb979144b39869e75f,,16fb979144b39869e75f,16394,Markers,32.36,2020-11-02 09:05:00 UTC,,,"
# {""source"":""api"",""session"":""be67a5b40c13c0ecf1d8f272455194a15b5e6806132a01bdaf86a0e95f469baf"","
# "viewport"":{""width"":1324,""height"":783},""started_at"":""2020-11-02T09:04:28.983Z"",""user_agent"":""Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7)
#  AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0 Safari/605.1.15"",""utc_offset"":""-3600"",""finished_at"":""2020-11-02T09:05:00.230Z"","
# "live_project"":true,""interventions"":{""opt_in"":false,""messageShown"":false},""user_language"":""en"",""subject_dimensions"":[null],"
# "subject_selection_state"":{""retired"":false,""selected_at"":""2020-11-02T08:56:00.628Z"",""already_seen"":false,""selection_state"":""normal"","
# "finished_workflow"":false,""user_has_finished_workflow"":false},""workflow_translation_id"":""37700""}","
# [{""task"":""T0"",""task_label"":""Nástroje na označenie"",""value"":[{""x"":799.8685302734375,""y"":0.015093331225216389,""tool"":0,""frame"":0,
# ""width"":20.07513427734375,""height"":158.77604436408728,""details"":[],""tool_label"":""Blesk""}]},{""task"":""T1"",""task_label"":
# ""Si si istý, že si oanotoval všetky blesky?"",""value"":""Áno, chcem pokračovať na ďalší obrázok""}]","{""51384993"":{""retired"":{""id"":71944463,
# ""workflow_id"":16394,""classifications_count"":2,""created_at"":""2020-11-01T20:27:51.933Z"",""updated_at"":""2020-11-02T09:05:00.512Z"",""retired_at"":
# ""2020-11-02T09:05:00.504Z"",""subject_id"":51384993,""retirement_reason"":""classification_count""},""Filename"":""ch1_20160814_151726_036_s143.png""}}",51384993

