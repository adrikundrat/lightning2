#ak
# import PyPDF2
import os
import tempfile
import zipfile,fnmatch

from PyPDF2 import PdfFileWriter, PdfFileReader
from pdf2image import convert_from_path, convert_from_bytes
from pdf2image.exceptions import (
    PDFInfoNotInstalledError,
    PDFPageCountError,
    PDFSyntaxError
)
from PIL import Image

#### ak822dj
#######################################################################
#### I know to unzip and clean folders with data of lightning
#### Then get and crop spectrograms from each pdf available
#######################################################################

rootPath = r"/Users/adrian/Desktop/skola3BP/BP/data/porezanedata/elm_20160809_002120_pics"

####### UNZIP
pattern = '*.zip'
for root, dirs, files in os.walk(rootPath):
    for filename in fnmatch.filter(files, pattern):
        print("####################################")
        print("unzipping %s" %filename)
        zipfile.ZipFile(os.path.join(root, filename)).extractall(os.path.join(root, os.path.splitext(filename)[0]))
        os.remove(os.path.join(root, filename))

for root, dirs, files in os.walk(rootPath):
    print("####################################")
    print("removing nopdfs")
    for file in files:
        if file.endswith(".pdf")==False:
            os.remove(os.path.join(root, file))


####### CROP
for root, dirs, files in os.walk(rootPath):
    pole=[]
    for file in files:
        if file.endswith(".png"):
            print("####################################")
            print("processing %s" %root)
            pole=[]
            pdfFileObj = open(os.path.join(root, file), 'rb')
            pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
            pageObjDate = pdfReader.getPage(0)
            textDate=pageObjDate.extractText()
            if "from " in textDate:
                datum=textDate[textDate.find('from ')+5:textDate.find('from ')+29]
            else: 
                print("pdf from %s is empty, lets continue")
                break
            datum=datum.replace("-", "")
            datum=datum.replace("T", "_")
            datum=datum.replace(".", "_", 1)
            datum=datum.replace(".", "")
            datum=datum.replace(":", "")
            for j in range(pdfReader.numPages):
                pageObj = pdfReader.getPage(j)
                text=pageObj.extractText()
                if "Part" in text:
                    pole.append(text[text.find('Part')+5:text.find('/')])
                else:
                    pole.append(0)
            
            
            #with tempfile.TemporaryDirectory() as path:
            images_from_path = convert_from_path(os.path.join(root, file))

            i=0
            for page in images_from_path:
                page.save(os.path.join(root,"%s. %s.png" % (i, pole[i])), 'PNG')   
                i+=1

            os.remove(os.path.join(root, "0. 0.png"))
            os.remove(os.path.join(root, "%s. %s.png" % (i-1, pole[i-1])))
            pole.remove(pole[0])
            pole.remove(pole[i-2])

            datum=root[root.find('elm_')+4:root.find('_pics')]
            for i in pole:
                im = Image.open(os.path.join(root, "%s" % (file)))
                im.crop((0, 84, 1421, 227)).save(os.path.join(root, "%s" % (file)))
            os.remove(os.path.join(root, "%s. %s.png" % (str(pole.index(i)+1), i)))
            print(root)
            os.remove(os.path.join(root, "data.pdf"))
            print("just made pngs from %s"%root)
        else:
            continue