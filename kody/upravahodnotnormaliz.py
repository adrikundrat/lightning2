# import pandas as pd
# import matplotlib.pyplot as plt
# # %matplotlib inline
# from matplotlib import patches
import csv
import pandas as pd
import numpy as np
import math 
import os
import tempfile
import cv2
import json
import zipfile,fnmatch
from PIL import Image, ImageDraw
from os import path

#### ak822dj
##############################
#### I know to make normalized txt annotations as input for yolov5 using zooniverse annotations
##############################

data=pd.read_csv("/Users/adrian/OneDrive - Technicka univerzita v Kosiciach/skola3BP/BP/vystupy_anot/jjmarkers-classifications2.csv")
data=data.drop(data.index[0:50])

a=50

with open('/home/jovyan/data/lightning/adrian/pics/tabulka10.csv', mode='w') as csv_file:
    fieldnames = ['label','x', 'y', 'width', 'height']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()
    while (a<1500):
        load=json.loads(data.iloc[a, 11])[0]["value"]
        filesy=json.loads(data.iloc[a,12])[str(data.iloc[a,13])]["Filename"]
        filesySplit=filesy.split(".")
        rootPath = r"/home/jovyan/data/lightning/adrian/pics/pics_zrezany_vrch/201"+filesy[7:8]+'/'+filesy[8:10]+'/'+filesy[10:12]
        print(rootPath)
        for root, dirs, files in os.walk(rootPath):
            # print(root)
            for file in files:
                if file == filesy:
                    im = Image.open(os.path.join(root, "%s" % (file)))
                    im.save(os.path.join("/home/jovyan/data/lightning/adrian/pics/trenovacieobr", "%s.jpg" % (filesySplit[0])))
        
        
        filesy=filesy.split(".")
        with open(os.path.join("/home/jovyan/data/lightning/adrian/pics/trenovacieobr/textaky", "%s.txt" % (filesy[0])), mode='w') as csv_file:
            fieldnames1 = ['label','x', 'y', 'width', 'height']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames1, delimiter=' ')
            for k in load:
                writer.writerow({'label':0, 'x':round((((k["x"])+(k["width"]/2))/1421),5), 'y':0.5, 'width':round((k["width"]/1421),5), 'height':1})                                             #height

        print(a)
        a+=1

