import pandas as pd
import json
import math 
from PIL import Image, ImageDraw
import os
import tempfile
import zipfile,fnmatch

#### ak822dj
########################
#### I know to draw black lines into real spectrograms due to annotations
########################

data=pd.read_csv("/data/lightning/adrian/scripts/annotations_check/jjmarkers-classifications2.csv")
data=data.drop(data.index[0:50])

a=0
while (a<=len(data)-1):
    load=json.loads(data.iloc[a, 11])[0]["value"]
    filesy=json.loads(data.iloc[a,12])[str(data.iloc[a,13])]["Filename"]
    rootPath = r"/data/lightning/adrian/pics/porezane/201"+filesy[7:8]+'/'+filesy[8:10]+'/'+filesy[10:12]

    for root, dirs, files in os.walk(rootPath):
        for file in files:
            if file == filesy:
                print(a)
                print(file)
                print("###########################################################################")
                im = Image.open(os.path.join(root, "%s" % (file)))
                # im1 = ImageDraw.Draw(im)
                for k in load:
                    print(k["x"])
                    shape=[(k["x"], 0), (k["x"], 162)]
                    im1.line(shape, fill="black", width=2)
                    shape=[(k["x"]+k["width"], 0), (k["x"]+k["width"], 162)]
                    im1.line(shape, fill="black", width=2)

                im.save(os.path.join("/data/lightning/adrian/pics/annotations", "1%s" % (file)))
        
    # print(a)
    a+=1


